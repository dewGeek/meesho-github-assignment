package com.halodoc.algolia.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by brainnr on 10/17/17.
 */

public class UserData implements Serializable {
    @SerializedName("avatar_url")
    private String avatarUrl;

    @SerializedName("login")
    private String login;

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public String getLogin() {
        return login;
    }
}

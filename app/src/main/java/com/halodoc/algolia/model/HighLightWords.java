package com.halodoc.algolia.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by brainnr on 8/30/17.
 */

public class HighLightWords implements Serializable {

    @SerializedName("matchedWords")
    private List<String> matchedWords;

    @SerializedName("value")
    private String value;

    public List<String> getMatchedWords() {
        return matchedWords;
    }
}

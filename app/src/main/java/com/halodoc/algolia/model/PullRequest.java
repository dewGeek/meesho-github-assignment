package com.halodoc.algolia.model;

import com.google.gson.annotations.SerializedName;
import com.halodoc.algolia.util.HelperFns;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by brainnr on 8/30/17.
 */

public class PullRequest implements Serializable {

    private String title;

    private String body;

    @SerializedName("assignee")
    private UserData assignee;

    @SerializedName("html_url")
    private String htmlUrl;

    public PullRequest() {
        assignee = new UserData();
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }


    public UserData getAssignee() {
        if(assignee == null) {
            assignee = new UserData();
        }
        return assignee;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    @Override
    public String toString() {
        return HelperFns.getGson().toJson(this);
    }
}

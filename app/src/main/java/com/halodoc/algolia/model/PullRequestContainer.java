package com.halodoc.algolia.model;

import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by brainnr on 8/30/17.
 */

public class PullRequestContainer implements Serializable {

    private static final String TAG = "PULL_REQUESTS";

    private List<PullRequest> pullRequestList;

    private transient DataSetObservable dataSetObservable;

    public PullRequestContainer() {
        pullRequestList = new ArrayList<>();
        dataSetObservable = new DataSetObservable();
    }

    public void appendPullRequests(List<PullRequest> pulls) {
        pullRequestList.addAll(pulls);
    }

    public List<PullRequest> getPullRequestList() {
        return pullRequestList;
    }

    public void setPullRequestList(List<PullRequest> pullRequestList) {
        this.pullRequestList = pullRequestList;
    }

    public void registerDatasetObserver(DataSetObserver dataSetObserver) {
        dataSetObservable.registerObserver(dataSetObserver);
    }

    public void unregisterDatasetObserver(DataSetObserver dataSetObserver) {
        dataSetObservable.unregisterObserver(dataSetObserver);
    }

    public void notifyObservers() {
        Log.d(TAG, "notifyObservers");
        dataSetObservable.notifyChanged();
    }
}


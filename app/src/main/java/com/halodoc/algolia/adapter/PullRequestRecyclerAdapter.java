package com.halodoc.algolia.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.halodoc.algolia.R;
import com.halodoc.algolia.fragment.PullReqestListFragment.OnListFragmentInteractionListener;
import com.halodoc.algolia.model.PullRequest;
import com.halodoc.algolia.model.UserData;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link PullRequest} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class PullRequestRecyclerAdapter extends RecyclerView.Adapter<PullRequestRecyclerAdapter.ViewHolder> {

    private final List<PullRequest> pullRequestList;
    private final OnListFragmentInteractionListener listener;
    private static final String TAG = "PULL_RECYCLER_ADAPTER";

    public PullRequestRecyclerAdapter(List<PullRequest> items, OnListFragmentInteractionListener listener) {
        pullRequestList = items;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_article, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Context context = holder.getContext();
        final PullRequest pullRequest = pullRequestList.get(position);
        UserData assignee = pullRequest.getAssignee();
        holder.contentView.setText(context.getString(R.string.label_author, assignee.getLogin()));

        String title = pullRequest.getTitle();

        holder.idView.setText(title);

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != listener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    listener.onListFragmentInteraction(pullRequest.getHtmlUrl());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return pullRequestList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final TextView idView;
        public final TextView contentView;
        public View view;
        private Context context;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            context = view.getContext();
            idView = (TextView) view.findViewById(R.id.id);
            contentView = (TextView) view.findViewById(R.id.content);
        }

        public Context getContext() {
            return context;
        }
    }
}

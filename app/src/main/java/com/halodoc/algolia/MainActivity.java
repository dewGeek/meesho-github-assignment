package com.halodoc.algolia;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.halodoc.algolia.fragment.PullReqestListFragment;
import com.halodoc.algolia.util.Constants;

public class MainActivity extends AppCompatActivity implements PullReqestListFragment.OnListFragmentInteractionListener {

    private static final String TAG_ARTICLE_LIST_FRAGMENT = "ARTICLE_LIST_FRAGMENT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setFragment(PullReqestListFragment.newInstance(), TAG_ARTICLE_LIST_FRAGMENT, R.id.fragment_container);
    }

    @Override
    public void onListFragmentInteraction(String articleLink) {
        Intent intent = new Intent(MainActivity.this, WebViewActivity.class);
        intent.putExtra(Constants.ARG_ARTICLE_URL, articleLink);
        startActivity(intent);
    }

    public <T extends Fragment> void setFragment(T fragment, String fragmentTag, int objectId) {

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        Fragment newFragment = fm.findFragmentByTag(fragmentTag);
        if (newFragment == null) {
            fragmentTransaction.replace(objectId, fragment, fragmentTag);
        }
        if (!isFinishing()) {
            fragmentTransaction.commitAllowingStateLoss();
        }
    }

}

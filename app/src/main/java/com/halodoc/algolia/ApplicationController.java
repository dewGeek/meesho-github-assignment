package com.halodoc.algolia;

import android.app.Application;

import com.halodoc.algolia.api.ApiClient;
import com.halodoc.algolia.api.ServiceGenerator;

/**
 * Created by brainnr on 8/30/17.
 */

public class ApplicationController extends Application {

    private static ApiClient client;

    @Override
    public void onCreate() {
        super.onCreate();
        setInstance();
    }

    private static synchronized void setInstance() {
        client = ServiceGenerator.createService(ApiClient.class);
    }

    public static ApiClient getClient() {
        return client;
    }
}

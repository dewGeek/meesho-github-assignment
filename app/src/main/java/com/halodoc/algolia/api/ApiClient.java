package com.halodoc.algolia.api;

import com.halodoc.algolia.model.PullRequest;
import com.halodoc.algolia.model.PullRequestContainer;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by brainnr on 8/30/17.
 */

public interface ApiClient {

    @GET("repos/{owner}/{repo}/pulls\n")
    Call<List<PullRequest>> loadPullRequests(
            @Path("owner") String owner,
            @Path("repo") String repo
    );
}

package com.halodoc.algolia.api;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.widget.Toast;

import com.halodoc.algolia.ApplicationController;
import com.halodoc.algolia.model.PullRequest;
import com.halodoc.algolia.model.PullRequestContainer;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by brainnr on 8/30/17.
 */

public class DataLoader {

    private static final String TAG = "DATALOADER";
    private ApiClient client;
    private Context context;

    public DataLoader(Context context) {
        client = ApplicationController.getClient();
        this.context = context;
    }

    public void searchPullRequests(String owner, final String repo, final PullRequestContainer pullRequestContainer) {
        Call<List<PullRequest>> searchNewsCall = client.loadPullRequests(owner, repo);
        searchNewsCall.enqueue(new Callback<List<PullRequest>>() {
            @Override
            public void onResponse(Call<List<PullRequest>> call, Response<List<PullRequest>> response) {
                if (response.isSuccessful()) {
                    pullRequestContainer.setPullRequestList(response.body());
                    pullRequestContainer.notifyObservers();
                } else {
                    if (context instanceof Activity) {
                        Toast.makeText(context, response.message(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<PullRequest>> call, Throwable t) {
                if (context instanceof Activity) {
                    Toast.makeText(context, "Unable to fetch results", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}

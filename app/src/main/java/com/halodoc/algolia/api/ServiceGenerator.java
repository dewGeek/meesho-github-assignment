package com.halodoc.algolia.api;


//import com.halodoc.algolia.ApplicationController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.halodoc.algolia.BuildConfig;
import com.halodoc.algolia.util.Constants;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by brainnr on 19/7/16.
 */
public class ServiceGenerator {

    public static <S> S createService(Class<S> serviceClass) {

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(BuildConfig.REST_HOST)
                .addConverterFactory(buildGsonConverter());

        Retrofit retrofit = builder.client(getOkHttpClient()).build();
        return retrofit.create(serviceClass);

    }

    private static GsonConverterFactory buildGsonConverter() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder
//                .setDateFormat(Constants.SERVER_DATE_FORMAT)
                .create();
        return GsonConverterFactory.create(gson);
    }

    private static OkHttpClient getOkHttpClient() {

        return new OkHttpClient.Builder()
                .readTimeout(BuildConfig.CONNECTION_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .connectTimeout(BuildConfig.CONNECTION_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .addInterceptor(
                        new HttpLoggingInterceptor().setLevel(BuildConfig.RESPONSE_LOG_LEVEL)
                )
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Interceptor.Chain chain) throws IOException {

                        Request original = chain.request();
                        Request.Builder requestBuilder = original.newBuilder();
                        requestBuilder.addHeader("Accept", "application/vnd.github.v3+json");
                        Request request = requestBuilder.build();
                        return chain.proceed(request);

                    }
                })

                .build();
    }
}

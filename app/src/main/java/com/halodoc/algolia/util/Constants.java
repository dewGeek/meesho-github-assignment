package com.halodoc.algolia.util;

/**
 * Created by brainnr on 8/30/17.
 */

public class Constants {

    public static final String SERVER_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.sTZD";
    public static final String ARG_ARTICLE_URL = "ARTICLE_URL";

}

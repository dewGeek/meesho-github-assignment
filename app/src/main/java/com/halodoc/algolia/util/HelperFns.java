package com.halodoc.algolia.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by brainnr on 8/30/17.
 */

public class HelperFns {

    public static Gson getGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();

        return gsonBuilder
//                .setDateFormat(Constants.SERVER_DATE_FORMAT)
                .create();
    }

}

package com.halodoc.algolia.fragment;

import android.content.Context;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.halodoc.algolia.R;
import com.halodoc.algolia.adapter.PullRequestRecyclerAdapter;
import com.halodoc.algolia.api.DataLoader;
import com.halodoc.algolia.model.PullRequestContainer;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class PullReqestListFragment extends Fragment {


    private OnListFragmentInteractionListener listener;
    private PullRequestContainer pullRequestContainer;
    private DataSetObserver dataSetObserver;

    // View Variables
    private RecyclerView pullRequestRecyclerView;
    private EditText searchEditText;
    private Button searchBtn;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public PullReqestListFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static PullReqestListFragment newInstance() {
        PullReqestListFragment fragment = new PullReqestListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pullRequestContainer = new PullRequestContainer();
        dataSetObserver = new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                updateUI();
            }
        };
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_article_list, container, false);

        // Set the adapter
        Context context = view.getContext();
        pullRequestRecyclerView = (RecyclerView) view.findViewById(R.id.list);
        pullRequestRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        pullRequestRecyclerView.setAdapter(new PullRequestRecyclerAdapter(pullRequestContainer.getPullRequestList(), listener));


        searchEditText = (EditText) view.findViewById(R.id.search_edit_text);
        searchEditText.setText("mozilla/treeherder");
        searchBtn = (Button) view.findViewById(R.id.btn_search);
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String searchQuery = searchEditText.getText().toString();
                if (!TextUtils.isEmpty(searchQuery)) {
                    final DataLoader dataLoader = new DataLoader(getContext());
                    String[] searchParts = searchQuery.split("/");
                    if (searchParts.length == 2 && !TextUtils.isEmpty(searchParts[0]) && !TextUtils.isEmpty(searchParts[1])) {
                        dataLoader.searchPullRequests(searchParts[0], searchParts[1], pullRequestContainer);
                    }

                }
            }
        });

        final DataLoader dataLoader = new DataLoader(getContext());
        dataLoader.searchPullRequests("mozilla", "treeherder", pullRequestContainer);

        return view;
    }


    protected void updateUI() {
        pullRequestRecyclerView.swapAdapter(
                new PullRequestRecyclerAdapter(pullRequestContainer.getPullRequestList(), listener),
                true);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            listener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        pullRequestContainer.registerDatasetObserver(dataSetObserver);
    }

    @Override
    public void onPause() {
        super.onPause();
        pullRequestContainer.unregisterDatasetObserver(dataSetObserver);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {

        void onListFragmentInteraction(String articleLink);
    }
}
